package com.meghan.accountconfigbusclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class AccountconfigbusclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountconfigbusclientApplication.class, args);
	}

}
